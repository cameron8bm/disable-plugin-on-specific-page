<?php
/*
Plugin Name: Deativate Plugin On Specific Page
Author: Cameron Jones
Version: 1.0
Author URI: https://cameronjonesweb.com.au
*/
 
add_filter( 'option_active_plugins', 'cameronjonesweb_disable_plugins_on_pages' );

function cameronjonesweb_disable_plugins_on_pages($plugins){
	
	//Check if the page slug is the page we want
    if( strpos($_SERVER['REQUEST_URI'], '/reset-password/') !== false ) {
 		
		//Check the plugin is active. Copy this line for each plugin.
        $key[] = array_search( 'wordpress-seo/wp-seo.php' , $plugins );
		
		foreach( $key as $k ) {
			if ( false !== $k ) {
				//Deactivate it
				unset( $plugins[$k] );
			}
		}
    }
 
    return $plugins;
}